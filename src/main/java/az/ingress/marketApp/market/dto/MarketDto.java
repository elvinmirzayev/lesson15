package az.ingress.marketApp.market.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketDto {
    @NotBlank
    String name;
    @NotBlank
    String address;
    @JsonIgnore
    @Valid
    List<BranchDto> branches;
}

