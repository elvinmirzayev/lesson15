package az.ingress.marketApp.market.dto;


import az.ingress.marketApp.market.model.Market;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonPropertyOrder({ "name", "address", "countOfEmployee","phones","manager","market"})
public class BranchDto {
    @NotBlank
    String name;
    @NotBlank
    String address;
    @JsonProperty("countofemployee")
    @Positive @Min(value = 1)
    Integer countOfEmployee;
    @Valid
    List<PhoneDto> phones;
    @Valid
    ManagerDto manager;
    @Valid
    Market market;

}


