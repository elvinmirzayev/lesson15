package az.ingress.marketApp.market.service;

import az.ingress.marketApp.market.dto.ManagerDto;
import az.ingress.marketApp.market.repositoty.genericsearch.SearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface ManagerService {
    ManagerDto createManager(ManagerDto managerDto);

    ManagerDto updateManager(Integer managerId, ManagerDto managerDto);


    Page<ManagerDto> findAll(Pageable pageable);

    ManagerDto findById(Integer id);
    void deleteManager(Integer id);

    Page<ManagerDto> searchManager(List<SearchCriteria> searchCriteriaList, Pageable pageable);
}



