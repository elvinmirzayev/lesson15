package az.ingress.marketApp.market.service;

import az.ingress.marketApp.market.dto.ManagerDto;
import az.ingress.marketApp.market.mapper.ManagerMapper;
import az.ingress.marketApp.market.model.Manager;
import az.ingress.marketApp.market.repositoty.ManagerRepository;
import az.ingress.marketApp.market.repositoty.genericsearch.CustomSpecification;
import az.ingress.marketApp.market.repositoty.genericsearch.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import java.util.List;



@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private final ManagerRepository managerRepository;
    private final ManagerMapper managerMapper;
    private final CustomSpecification<Manager> specification;

    @Override
    public ManagerDto createManager(ManagerDto managerDto) {
        Manager manager = managerMapper.mapToManager(managerDto);
        Manager savedManager = managerRepository.save(manager);
        return managerMapper.mapToManagerDto(savedManager);
    }

    @Override
    public ManagerDto updateManager(Integer managerId, ManagerDto managerDto) {
        Manager managerDb = managerRepository.cFindById(managerId).orElseThrow(RuntimeException::new);
        Manager manager = managerMapper.mapToManager(managerDto);
        manager.setId(managerId);
        if (managerDto.getPhones().isEmpty()) {
            manager.setPhones(managerDb.getPhones());
        }
        Manager savedManager = managerRepository.save(manager);
        return managerMapper.mapToManagerDto(savedManager);
    }

    @Override
    public Page<ManagerDto> findAll(Pageable pageable) {
        return managerMapper.mapToManagerDtoPage(managerRepository.cFindAll(pageable));
    }

    @Override
    public ManagerDto findById(Integer id) {
        Manager managerDb = managerRepository.cFindById(id).orElseThrow(RuntimeException::new);
        return managerMapper.mapToManagerDto(managerDb);    }

    @Override
    public void deleteManager(Integer id) {
        managerRepository.cFindById(id).orElseThrow(RuntimeException::new);
        managerRepository.deleteById(id);
    }

    @Override
    public Page<ManagerDto> searchManager(List<SearchCriteria> searchCriteriaList, Pageable pageable) {
        specification.setCriteriaList(searchCriteriaList);
        return managerMapper.mapToManagerDtoPage(managerRepository.findAll((Specification<Manager>) specification,pageable));
    }
}



