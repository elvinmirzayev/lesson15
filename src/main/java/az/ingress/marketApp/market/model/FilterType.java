package az.ingress.marketApp.market.model;

public enum FilterType {
    EQUALS,
    START_WITH,
    ENDS_WITH
}
