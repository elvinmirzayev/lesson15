package az.ingress.marketApp.market.mapper;


import az.ingress.marketApp.market.dto.ManagerDto;
import az.ingress.marketApp.market.model.Manager;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ManagerMapper {
    ManagerDto mapToManagerDto(Manager manager);
    Manager mapToManager(ManagerDto managerDto);
    List<ManagerDto> mapToManagerDtoList(List<Manager> taskList);

    default Page<ManagerDto> mapToManagerDtoPage(Page<Manager> managerPage) {
        return managerPage.map(this::mapToManagerDto);
    }

}
