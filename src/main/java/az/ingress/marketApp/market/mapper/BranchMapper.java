package az.ingress.marketApp.market.mapper;

import az.ingress.marketApp.market.dto.BranchDto;
import az.ingress.marketApp.market.model.Branch;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BranchMapper {
    BranchDto mapToBranchDto(Branch branch);

    Branch mapToBranch(BranchDto branchDto);

    List<BranchDto> mapToBranchDtoList(List<Branch> branchList);
}


