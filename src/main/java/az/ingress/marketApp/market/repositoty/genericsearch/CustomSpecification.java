package az.ingress.marketApp.market.repositoty.genericsearch;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.List;
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
public class CustomSpecification<T>{
    List<SearchCriteria> criteriaList;

    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = criteriaBuilder.conjunction();

        for (SearchCriteria criteria : criteriaList) {
            if (criteria.getOperation().equals(SearchOperation.EQUAL)) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue())
                );
            }  else if (criteria.getOperation().equals(SearchOperation.NOT_EQUAL)) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.notEqual(root.get(criteria.getKey()), criteria.getValue()));
            }
            else if (criteria.getOperation().equals(SearchOperation.IN)) {
                predicate = criteriaBuilder.and(
                        predicate,
                        criteriaBuilder.in(root.get(criteria.getKey())).value(criteria.getValue()));
            }
            else if (criteria.getOperation().equals(SearchOperation.NOT_IN)) {

                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.not(root.get(criteria.getKey())).in(criteria.getValue()));
            }
            else if (criteria.getOperation().equals(SearchOperation.MATCH_START)) {

                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.like(
                                criteriaBuilder.lower(root.get(criteria.getKey())),
                                criteria.getValue().toString().toLowerCase() + "%"));
            }
            else if (criteria.getOperation().equals(SearchOperation.MATCH_END)) {

                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.like(
                                criteriaBuilder.lower(root.get(criteria.getKey())),
                                "%" + criteria.getValue().toString().toLowerCase()));
            }
            else if (criteria.getOperation().equals(SearchOperation.MATCH)) {
                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.like(
                                criteriaBuilder.lower(root.get(criteria.getKey())),
                                "%" + criteria.getValue().toString().toLowerCase() + "%"));
            }
            else if (criteria.getOperation().equals(SearchOperation.GREATER_THAN)) {
                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.greaterThan(
                                root.get(criteria.getKey()), criteria.getValue().toString()));
            }
            else if (criteria.getOperation().equals(SearchOperation.LESS_THAN)) {
                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.lessThan(
                                root.get(criteria.getKey()), criteria.getValue().toString()));
            }
            else if (criteria.getOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.greaterThanOrEqualTo(
                                root.get(criteria.getKey()), criteria.getValue().toString()));
            }
            else if (criteria.getOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
                predicate = criteriaBuilder.and(
                        predicate,criteriaBuilder.lessThanOrEqualTo(
                                root.get(criteria.getKey()), criteria.getValue().toString()));
            }
        }
        return predicate;
    }
}

