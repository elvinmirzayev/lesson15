package az.ingress.marketApp.market.repositoty;

import az.ingress.marketApp.market.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.Optional;

public interface MarketRepository extends JpaRepository<Market,Integer> {
    @Query(value = "select b from Market b left join fetch b.branches where b.id=:id")
    Optional<Market> cFindById(Integer id);

    @Query(value = "select b from Market b left join fetch b.branches")
    List<Market> cFindByAll();
}
