package az.ingress.marketApp.market.repositoty;

import az.ingress.marketApp.market.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.Optional;

public interface BranchRepository extends JpaRepository<Branch,Integer> {

    @Query(value = "select b from Branch b left join fetch b.manager left join fetch b.market left join b.phones where b.id=:id")
    Optional<Branch> cFindById(Integer id);

    @Query(value = "select b from Branch b left join fetch b.manager left join fetch b.market left join b.phones")
    List<Branch> cFindAll();
}


