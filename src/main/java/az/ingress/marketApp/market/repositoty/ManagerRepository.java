package az.ingress.marketApp.market.repositoty;

import az.ingress.marketApp.market.model.Manager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ManagerRepository extends JpaRepository<Manager,Integer>, JpaSpecificationExecutor<Manager> {
    @Query(value = "select b from Manager b left join fetch b.phones where b.id=:id")
    Optional<Manager> cFindById(Integer id);

    @Query(value = "select b from Manager b left join fetch b.phones")
    Page<Manager> cFindAll(Pageable pageable);

    @EntityGraph(attributePaths = "phones",type = EntityGraph.EntityGraphType.FETCH)
    Page<Manager> findAll(Specification<Manager> specification, Pageable pageable);
}

